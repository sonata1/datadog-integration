locals {
  datadog_tags = [for key, value in var.datadog_tags : "${key}:${value}"]
}

resource "datadog_integration_aws" "integration" {
  count = var.enabled ? 1 : 0

  account_id       = var.account_id
  excluded_regions = var.excluded_regions
  role_name        = "DatadogAWSIntegrationRole"

  host_tags = flatten(concat([
    [format("Account_ID:%s", var.account_id), "Infra:AWS"],
    local.datadog_tags
  ]))

  account_specific_namespace_rules = var.account_specific_datadog_namespace_rules
  # https://docs.datadoghq.com/integrations/amazon_web_services/#cloud-security-posture-management
  cspm_resource_collection_enabled = true # reqires IAM policy assigned to DD role
  metrics_collection_enabled       = true
  resource_collection_enabled      = true
}
