# Datadog-Integration

## Summary
This is the module that integrates your AWS account with Datadog

## Usage
Since version 2022.11.16 the module has been updated to use implicit provider passing.

Example usage below:

```
provider "aws" {
  region = var.region
  alias  = "core_shared"
  assume_role {
    role_arn = "arn:aws:iam::512426816668:role/ReadDatadogKeys"
  }
}

provider "datadog" {
  api_url = "https://api.datadoghq.eu/"
  api_key = data.aws_secretsmanager_secret_version.datadog_api_key.secret_string
  app_key = data.aws_secretsmanager_secret_version.datadog_app_key.secret_string
}

data "aws_secretsmanager_secret_version" "datadog_api_key" {
  secret_id = "arn:aws:secretsmanager:eu-west-2:512426816668:secret:/prod/root/datadog/api_key-1tsRdO"
  provider  = aws.core_shared
}

data "aws_secretsmanager_secret_version" "datadog_app_key" {
  secret_id = "arn:aws:secretsmanager:eu-west-2:512426816668:secret:/prod/root/datadog/app_key-PtwBEP"
  provider  = aws.core_shared
}

module "datadog-integration" {
  source       = "./modules/datadog"
  account_id   = var.account_id
  region       = var.region
  tags         = var.tags
  datadog_tags = var.datadog_tags
  account_specific_datadog_namespace_rules = {
    collect_custom_metrics = true
    workspaces             = true
  }
}

```

`account_specific_datadog_namespace_rules` (map of boolean) enables or 
disables metric collection for specific AWS namespaces for this AWS 
account only. A list of namespaces can be found at the 
[available namespace rules API endpoint](https://docs.datadoghq.com/api/latest/aws-integration/#list-namespace-rules)

`default_tags` (map of strings) default tags taht will be mapped to Datatadog tags.
Apart form default tags, module will automatically include following tags:
* `Account_ID:<AWS account ID>` 
* `Infra:AWS`

Limitations:
This module is intended to be used within your GitLab CI/CD pipeline, using
GEL standardized [GitLab runners](https://gitlab.com/genomicsengland/cloud/aws-core/shared/gitlab-runner)

<!-- BEGIN_TF_DOCS -->
## Requirements

No requirements.

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | n/a |
| <a name="provider_datadog"></a> [datadog](#provider\_datadog) | n/a |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_iam_policy.datadog_aws_integration](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy) | resource |
| [aws_iam_role.datadog_aws_integration](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_iam_role_policy_attachment.datadog_aws_integration](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |
| [aws_iam_role_policy_attachment.datadog_security_audit](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |
| [datadog_integration_aws.integration](https://registry.terraform.io/providers/DataDog/datadog/latest/docs/resources/integration_aws) | resource |
| [aws_iam_policy_document.datadog_aws_integration](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_iam_policy_document.trust_datadog](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [datadog_ip_ranges.this](https://registry.terraform.io/providers/DataDog/datadog/latest/docs/data-sources/ip_ranges) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_account_id"></a> [account\_id](#input\_account\_id) | AWS account id | `string` | n/a | yes |
| <a name="input_account_specific_datadog_namespace_rules"></a> [account\_specific\_datadog\_namespace\_rules](#input\_account\_specific\_datadog\_namespace\_rules) | Enables or disables metric collection for specific AWS namespaces for this AWS account only | `map(bool)` | n/a | yes |
| <a name="input_datadog_tags"></a> [datadog\_tags](#input\_datadog\_tags) | Tags to apply to the Datadog integration | `map(any)` | `{}` | no |
| <a name="input_enabled"></a> [enabled](#input\_enabled) | Toggles creation of resources and data sources in this repository | `bool` | `true` | no |
| <a name="input_excluded_regions"></a> [excluded\_regions](#input\_excluded\_regions) | An array of AWS regions to exclude from metrics collection. | `list(any)` | <pre>[<br>  "ap-south-1",<br>  "eu-north-1",<br>  "eu-west-3",<br>  "ap-northeast-3",<br>  "ap-northeast-2",<br>  "ap-northeast-1",<br>  "ca-central-1",<br>  "sa-east-1",<br>  "ap-southeast-1",<br>  "ap-southeast-2",<br>  "eu-central-1",<br>  "us-east-2",<br>  "us-west-1",<br>  "us-west-2"<br>]</pre> | no |
| <a name="input_region"></a> [region](#input\_region) | AWS region | `string` | n/a | yes |
| <a name="input_tags"></a> [tags](#input\_tags) | Tags to apply to AWS resources associated with the Datadog integration | `map(any)` | `{}` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_datadog_ip_ranges"></a> [datadog\_ip\_ranges](#output\_datadog\_ip\_ranges) | n/a |
<!-- END_TF_DOCS -->