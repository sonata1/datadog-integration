variable "enabled" {
  type        = bool
  description = "Toggles creation of resources and data sources in this repository"
  default     = true
}

variable "account_id" {
  type        = string
  description = "AWS account id"
}

variable "region" {
  type        = string
  description = "AWS region"
}

variable "excluded_regions" {
  type        = list(any)
  description = "An array of AWS regions to exclude from metrics collection."
  default = [
    "ap-south-1",
    "eu-north-1",
    "eu-west-3",
    "ap-northeast-3",
    "ap-northeast-2",
    "ap-northeast-1",
    "ca-central-1",
    "sa-east-1",
    "ap-southeast-1",
    "ap-southeast-2",
    "eu-central-1",
    "us-east-2",
    "us-west-1",
    "us-west-2"
  ]
}

variable "account_specific_datadog_namespace_rules" {
  type        = map(bool)
  description = "Enables or disables metric collection for specific AWS namespaces for this AWS account only"
}

variable "tags" {
  type        = map(any)
  default     = {}
  description = "Tags to apply to AWS resources associated with the Datadog integration"
}

variable "datadog_tags" {
  type        = map(any)
  default     = {}
  description = "Tags to apply to the Datadog integration"
}
